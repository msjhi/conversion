package sn.mara.web.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RestResource {

    @GetMapping("/test1")
    public String getTest(){
        return "hello world";
    }
}
